
Typesafe queries for mongodb (an experiment)
===
Idea by Daniel Dietrich (https://twitter.com/danieldietrich/status/1082694592053547008).


Given a model like this:

```typescript
interface User {
    age: number;
}
```

A mongodb query may look like this:

````typescript
const query = {age: {$gte: 18}};
````

By adding an explicit type to _query_ we can use the compiler to check for certain errors.

Fail on unknown operators:

````typescript
const query: Query<User> = {age: {$gle: 13}} // compiler error
````

Fail on unknown properties:
````typescript
const query: Query<User> = {aage: 18} // compiler error
````

Fail on wrong type:
````typescript
const query: Query<User> = {age: "18"} // compiler error
````

Works with embedded objects:
````typescript
interface User {
    age: number;
    embedded: Embedded;
}
interface Embedded{
    id: string;
}

const query: Query<User> = {embedded: {id: "1"}}
````

Currently ````{embedded: {id: {$eq: "1"}}} ```` compiles, but is not supported by mongodb.
There are probably a lot of other false positives and false negatives.
E.g. accessing nested objects via ````{"embedded.id": "1"}```` is not supported.

Definition _Query_ can be found here: [src/index.ts](src/index.ts).

Sample queries: [test/queries.test.ts](test/queries.test.ts).

Integration tests here: [test/mongodb.test.ts](test/mongodb.test.ts).
