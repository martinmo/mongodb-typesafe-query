export interface User {
    age: number;
    name: string;
    embedded: Embedded;
    tags: string[];
}

export interface Embedded {
    id: string
}

function randomString(prefix: string): string {
    return prefix + "-" + Date.now()
}

function randomInt(from: number = 1, to: number = 100): number {
    return Math.floor(Math.random() * to) + from
}

export function aUser(user?: Partial<User>): User {
    const defaults: User = {
        age: randomInt(),
        name: randomString("Adam"),
        embedded: aEmbedded(),
        tags: []
    };
    return Object.assign(defaults, user);
}

export function aEmbedded(embedded?: Partial<Embedded>): Embedded {
    const defaults: Embedded = {id: randomString("eid")};
    return Object.assign(defaults, embedded);
}