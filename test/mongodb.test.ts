import {Collection, Db, MongoClient} from "mongodb";
import "mocha";
import {expect} from "chai";
import {aUser, User} from "./model";
import {connectDb, prepareUserCollection} from "./db";
import {Query} from "../src";

describe("Query MongoDB", () => {

    let mongo: { db: Db, client: MongoClient };
    let users: Collection;

    it("can do a simple query", async () => {
        await users.insertOne(aUser({name: "Adam"}));
        await users.insertOne(aUser());

        const query: Query<User> = {
            name: "Adam"
        };

        const found = await users.find(query).toArray();
        expect(found).length(1);
    });

    it("can do a query with operators", async () => {
        await users.insertOne(aUser({age: 10}));
        await users.insertOne(aUser({age: 16, name: "teenager"}));
        await users.insertOne(aUser({age: 33}));

        const query: Query<User> = {
            $and: [{age: {$gte: 13}}, {age: {$lte: 19}}]
        };

        const found = await users.find(query).toArray();
        expect(found).length(1);
        expect(found[0].name).to.eq("teenager");
    });

    it("can do a query on nested objects", async () => {
        await users.insertOne(aUser({embedded: {id: "1"}}));

        const query: Query<User> = {
            embedded: {id: "1"}
        };

        const found = await users.find(query).toArray();
        expect(found).length(1);
    });

    // xit("can do a complex query on nested objects", async () => {
    //     await users.insertOne(aUser({embedded: {id: "1"}}));
    //
    //     const query: Query<User> = {
    //         embedded: {id: {$eq: "1"}}
    //     };
    //
    //     const all = await users.find(query).toArray();
    //     expect(all).length(1);
    // });

    it("can do an all query on arrays", async () => {
        await users.insertOne(aUser({tags: ["a", "b", "c"]}));
        await users.insertOne(aUser({tags: ["a"]}));

        const query: Query<User> = {
            tags: {$all: ["a", "b"]}
        };

        const found = await users.find(query).toArray();
        expect(found).length(1);
    });

    it("can do a size query on arrays", async () => {
        await users.insertOne(aUser({tags: ["a", "b", "c"]}));
        await users.insertOne(aUser({tags: ["a"]}));

        const query: Query<User> = {
            tags: {$size: 3}
        };

        const found = await users.find(query).toArray();
        expect(found).length(1);
    });

    it("can do an matchAll query on arrays", async () => {
        await users.insertOne(aUser({tags: ["a"]}));
        await users.insertOne(aUser({tags: ["a", "b"]}));

        const query: Query<User> = {
            tags: {$elemMatch: {$eq: "b"}}
        };

        const found = await users.find(query).toArray();
        expect(found).length(1);
    });

    before(async () => {
        mongo = await connectDb();
    });

    beforeEach(async () => {
        users = await prepareUserCollection(mongo.db);
    });

    after(async () => {
        await mongo.client.close();
    });
});
