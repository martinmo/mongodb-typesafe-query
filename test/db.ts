import {Collection, Db, MongoClient} from "mongodb";

export async function connectDb(): Promise<{ db: Db, client: MongoClient }> {
    const url = 'mongodb://127.0.0.1:27017';
    const dbName = 'query-example';
    const client = await new MongoClient(url, {useNewUrlParser: true}).connect();
    return {db: client.db(dbName), client: client};
}

export async function prepareUserCollection(db: Db): Promise<Collection> {
    const users = db.collection("users");
    await users.deleteMany({});
    return users;
}