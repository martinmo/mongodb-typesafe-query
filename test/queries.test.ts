import "mocha";
import {User} from "./model";
import {Query} from "../src";


function findUser(_: Query<User>) {
    //
}

describe("Sample Queries", () => {

    it("should compile", () => {

        findUser({name: "1"});

        findUser({
            embedded: {
                id: {$eq: "1"}
            }
        });

        findUser({
            name: {$eq: "1"}
        });

        findUser({
            name: {$in: ["1"]}
        });

        findUser({
            "$or": [
                {name: {$eq: "1"}},
                {name: {$eq: "2"}}
            ]
        });

        findUser({name: {$type: "string"}});

        findUser({
            tags: {
                $all: ["admin"]
            }
        });

        findUser({tags: {$size: 1}});

        findUser({tags: {$elemMatch: {$eq: "admin"}}});
    });

});
