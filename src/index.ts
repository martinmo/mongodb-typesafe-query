export type Query<T> =
    { [K in keyof T]?: Criteria<T[K]> }
    | { [_ in '$and' | '$not' | '$nor' | '$or']?: Array<Query<T>> }

export type Criteria<T> =
    Query<T>
    | { [_ in '$eq' | '$ne' | '$gt' | '$gte' | '$lt' | '$lte']?: T | null }
    | { [_ in '$in' | '$nin']?: T[] }
    | { '$exists': boolean }
    | { '$type': BsonType }
    | { '$all': Array<IfArrayUnbox<T>> }
    | { '$size': IfArray<T, number> }
    | { '$elemMatch': Criteria<IfArrayUnbox<T>> }
    ;

export type IfArray<T, U> = T extends Array<unknown> ? U : never;
export type IfArrayUnbox<T> = T extends Array<infer U> ? U : never;

export type BsonType =
    "double" |
    "string" |
    "object" |
    "array" |
    "binData" |
    "undefined" |
    "objectId" |
    "bool" |
    "date" |
    "null" |
    "regex" |
    "dbPointer" |
    "javascript" |
    "symbol" |
    "javascriptWithScope" |
    "int" |
    "timestamp" |
    "long" |
    "decimal" |
    "minKey" |
    "maxKey";
